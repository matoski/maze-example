package main

import "fmt"

func main() {
	maze := [][]int{
		[]int{0, 1, 0, 0, 0, 0},
		[]int{0, 1, 0, 1, 1, 0},
		[]int{0, 1, 0, 1, 0, 0},
		[]int{0, 1, 1, 1, 2, 0},
		[]int{0, 0, 0, 0, 0, 0},
	}

	if steps, err := FindExit(maze); err == nil {
		fmt.Println(" OK : Exit found")
		for step := 0; steps.Len() > 0; step++ {
			e := steps.Front()
			fmt.Println("Step", step, ":", e.Value)
			steps.Remove(e)
		}
	} else {
		fmt.Println(err)
	}
}
