package main

import (
	"container/list"
	"fmt"
)

// FindExit traverse the maze from the first found starting cell (having value 2),
// and follows the empty cells (that have value 1) until it reaches the edge of the maze or dead end.
func FindExit(maze [][]int) (*list.List, error) {
	visited, err := createVisitedMaze(maze)
	if err != nil {
		return nil, err
	}

	steps := list.New()
	for y, row := range maze {
		for x, cell := range row {
			if cell == 2 { // start cell
				if found := findPath(maze, x, y, visited, steps); found {
					return steps, nil // exit found
				}
			}
		}
	}

	return nil, fmt.Errorf(" KO : Exit not found")
}

func findPath(maze [][]int, x int, y int, visited [][]bool, steps *list.List) bool {
	if !visited[y][x] {
		visited[y][x] = true // mark the cell as visited

		if y == 0 || y == (len(maze)-1) || x == 0 || x == (len(maze[0])-1) {
			steps.PushFront(fmt.Sprintf("(%d, %d)", x, y))
			return true // we are at the maze edge
		}

		if maze[y-1][x] > 0 && findPath(maze, x, y-1, visited, steps) { // search up unless there is a wall
			steps.PushFront(fmt.Sprintf("(%d, %d)", x, y))
			return true
		}

		if maze[y+1][x] > 0 && findPath(maze, x, y+1, visited, steps) { // search down unless there is a wall
			steps.PushFront(fmt.Sprintf("(%d, %d)", x, y))
			return true
		}
		if maze[y][x-1] > 0 && findPath(maze, x-1, y, visited, steps) { // search left unless there is a wall
			steps.PushFront(fmt.Sprintf("(%d, %d)", x, y))
			return true
		}
		if maze[y][x+1] > 0 && findPath(maze, x+1, y, visited, steps) { // search right unless there is a wall
			steps.PushFront(fmt.Sprintf("(%d, %d)", x, y))
			return true
		}
	}
	return false
}

func createVisitedMaze(maze [][]int) ([][]bool, error) {
	m := len(maze)
	if m == 0 {
		return nil, fmt.Errorf("Empty maze")
	}
	n := len(maze[0])
	if n == 0 {
		return nil, fmt.Errorf("Empty maze")
	}
	visited := make([][]bool, m)
	for i := range visited {
		visited[i] = make([]bool, n)
	}
	return visited, nil
}
