# Maze solving challenge

A maze consists of a block of cells, defined as a n by m matrix. The cells within the maze are either X, O, or S. An S denotes the starting point, an O denotes an open cell which can be passed through, and an X denotes a wall, which cannot be passed through. The game is complete when the S reaches the edge of the maze. Consider the following example of a 6 * 5 maze:

```bash
XOXXXX
XOXOOX
XOXOXX
XOOOSX
XXXXXX
```

Write code in a language of your choice (preferably Python) which will solve mazes similar to the one in the example above. You do not have to find the shortest path to the exit.

Optional bonus tasks you could attempt or describe how you would approach:

* Find the shorter path
* Find all paths
* Optimise your algorithm to find a path (any path) as quickly as possible
* Expand to three dimensional mazes

Deliver as source code with a readme explaining how to execute it. It is expected to spend no more than a few hours on this task.

## Solution

Give each type of cell a value.

* X = 0 (wall)
* O = 1 (empty cell)
* S = 2 (start cell)

The example maze would become

```go
M[5][6] = [
    [0, 1, 0, 0, 0, 0],
    [0, 1, 0, 1, 1, 0],
    [0, 1, 0, 1, 0, 0],
    [0, 1, 1, 1, 2, 0],
    [0, 0, 0, 0, 0, 0],
]
```

### Simple Solution

The simple solution is to find the index of the start cell in the maze and then recursively find a path from that index to the edge of the maze, by traversing the neighboring cells that are not walls (X = 0).

This solution is based on [Lee Algorithm](http://users.eecs.northwestern.edu/~haizhou/357/lec6.pdf) for maze router and it already finds the shorter path.

To find all paths we would need to allow the algorithm to execute all possible paths and keep track of the successful paths.

This can be extended to 3D maze by extending the data structure to 3 dimensional array and the algorithm to search on all three dimensions (x, y, z).

The algorithm can be optimized by changing the data structure to a linked graph, where each cell is a node and each boundary between adjacent cells is an edge.

## Instructions

The repository has CI pipeline that runs tests with several mazes. But you run the program locally using these steps:

```bash
go get -v
go build -v
./maze-example
```

The program will output

```bash
OK : Exit found
Step 0 : (4, 3)
Step 1 : (3, 3)
Step 2 : (2, 3)
Step 3 : (1, 3)
Step 4 : (1, 2)
Step 5 : (1, 1)
Step 6 : (1, 0)
```

For more maze examples see [maze_test.go](https://bitbucket.org/matoski/maze-example/src/master/maze_test.go).
