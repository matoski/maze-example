package main

import (
	"fmt"
	"testing"
)

func TestFindExit(t *testing.T) {
	testCases := []struct {
		desc string
		maze [][]int
		exp  error
	}{
		{
			desc: "No Path",
			maze: [][]int{
				[]int{0, 0, 0, 0, 0, 0},
				[]int{0, 1, 0, 1, 1, 0},
				[]int{0, 1, 0, 1, 0, 0},
				[]int{0, 1, 1, 1, 2, 0},
				[]int{0, 0, 0, 0, 0, 0},
			},
			exp: fmt.Errorf(" KO : Exit not found"),
		},
		{
			desc: "Challenge maze",
			maze: [][]int{
				[]int{0, 1, 0, 0, 0, 0},
				[]int{0, 1, 0, 1, 1, 0},
				[]int{0, 1, 0, 1, 0, 0},
				[]int{0, 1, 1, 1, 2, 0},
				[]int{0, 0, 0, 0, 0, 0},
			},
			exp: nil,
		},
		{
			desc: "Dual path",
			maze: [][]int{
				[]int{0, 0, 0, 0, 0, 0},
				[]int{0, 1, 1, 1, 1, 1},
				[]int{0, 1, 0, 1, 0, 0},
				[]int{0, 1, 1, 1, 2, 0},
				[]int{0, 0, 0, 0, 0, 0},
			},
			exp: nil,
		},
		{
			desc: "Dual exit",
			maze: [][]int{
				[]int{0, 1, 0, 0, 0, 0},
				[]int{0, 1, 0, 1, 1, 1},
				[]int{0, 1, 0, 1, 0, 0},
				[]int{0, 1, 1, 1, 2, 0},
				[]int{0, 0, 0, 0, 0, 0},
			},
			exp: nil,
		},
		{
			desc: "Start on the edge",
			maze: [][]int{
				[]int{0, 0, 0, 0, 0, 0},
				[]int{0, 1, 0, 1, 1, 1},
				[]int{0, 1, 0, 1, 0, 0},
				[]int{0, 1, 1, 1, 1, 2},
				[]int{0, 0, 0, 0, 0, 0},
			},
			exp: nil,
		},
		{
			desc: "Multiple starts",
			maze: [][]int{
				[]int{0, 1, 0, 0, 0, 0},
				[]int{0, 1, 0, 1, 1, 0},
				[]int{0, 1, 2, 1, 0, 0},
				[]int{0, 1, 1, 1, 2, 0},
				[]int{0, 0, 0, 0, 0, 0},
			},
			exp: nil,
		},
		{
			desc: "Larger maze",
			maze: [][]int{
				[]int{0, 0, 0, 0, 0, 0},
				[]int{0, 1, 0, 1, 1, 0},
				[]int{0, 1, 0, 1, 0, 0},
				[]int{0, 1, 1, 1, 2, 0},
				[]int{0, 0, 1, 0, 0, 0},
				[]int{0, 0, 1, 1, 1, 0},
				[]int{0, 0, 0, 0, 1, 0},
				[]int{0, 1, 1, 1, 1, 0},
				[]int{0, 1, 0, 0, 0, 0},
			},
			exp: nil,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			_, err := FindExit(tC.maze)
			equals(t, tC.exp, err)
		})
	}
}
